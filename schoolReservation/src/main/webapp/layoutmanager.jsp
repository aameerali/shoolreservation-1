    <%@ page contentType="text/html; charset=UTF-8"%>
    <%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>  
    <%@ taglib prefix="s" uri="/struts-tags"%>
   <!DOCTYPE html> 
    <html>  
    <head>  
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <%
  String contextPath = request.getContextPath();

%> 
    
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
   
    
    <!-- Bootstrap -->
    <link href="<%=contextPath%>/css/bootstrap.min.css" rel="stylesheet">
     <link href="<%=contextPath%>/css/font-awesome.min.css" rel="stylesheet" />
    
      <link href="<%=contextPath%>/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
       <link href="<%=contextPath%>/css/bootstrap-datepicker.min.css" rel="stylesheet">
       <link href="<%=contextPath%>/css/common.css" rel="stylesheet">
       <link href="<%=contextPath%>/css/leftMenuaccordion.css" rel="stylesheet">
       
     <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
     <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<%--      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> --%>
    
     <!-- Include all compiled plugins (below), or include individual files as needed -->
     
    
 <script src="<%=contextPath%>/jquery/jquery-1.11.3.min.js"></script>
 <script src="<%=contextPath%>/js/moment.js"></script>
  <script src="<%=contextPath%>/js/transition.js"></script>
<script src="<%=contextPath%>/js/collapse.js"></script>

<script src="<%=contextPath%>/js/bootstrap.min.js"></script>
<script src="<%=contextPath%>/js/bootstrap-datepicker.min.js"></script>

 <script src="<%=contextPath%>/js/respond.min.js"></script>
<script src="<%=contextPath%>/js/html5shiv.min.js"></script>
    </head>  
    <body  >  
      
      <nav class="navbar navbar-inverse navbar-fixed-top" style="background-color: #003366;">
  <div class="container-fluid">
    <div class="navbar-header">
      <img src="<%=contextPath%>/images/logo.png" class="img-thumbnail" alt="Cinque Terre"  height="100" width ="250">
    </div>
    
    <span class="label"><h5 style="margin-bottom: 0px;text-align: right;padding-right: 30px;">Welcome Arshad Backer</h5></span>
    <div style="text-align: right;padding-right: 30px;"><a href="#"><span class="label" style="font-size: 12px;" ><i class="fa fa-sign-out" ></i>Sign Out</span></a></div>
    <div style=" float:left;margin-left: 50px;"> <a href="<%=contextPath%>/homePageAction.action" style="color: white;" ><i class="fa fa-home fa-3x"  ></i></a> 
   </div>
  </div>
</nav>

<!-- Left Side menu starts -->

<!-- 			<div class="nav-side-menu" >
    
  
      <div class="menu-list">
  
            <ul id="menu-content" class="menu-content collapse out">
                <li class="collapsed active">
                  <a href="#" >
                  <i class="fa fa-calendar" aria-hidden="true"></i><s:text name="Calendar-en" />
                  </a>
                </li>

                <li  data-toggle="collapse" data-target="#products" >
                  <a href="#"><i class="fa fa-tasks" aria-hidden="true"></i> <s:text name="reservation-en"> </s:text> <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="products">
                    <li class="active"><a href="#"><i class="fa fa-plus-circle" aria-hidden="true"></i><s:text name="reservation.newreservation-en"> </s:text></li>
                    <li><a href="#"><i class="fa fa-search" aria-hidden="true"></i><s:text name="reservation.reservationsearch-en"> </s:text></a></li>
                    
                </ul>
                
                <li data-toggle="collapse" data-target="#schools" class="collapsed">
                  <a href="#"><i class="fa fa-university" aria-hidden="true"></i> <s:text name="schools-en"></s:text> <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="schools">
                  <li><a href="#"><i class="fa fa-plus-circle" aria-hidden="true"></i> <s:text name="schools.addschool-en"></s:text> </a></li>
                  <li> <a href="#"><i class="fa fa-search" aria-hidden="true"></i> <s:text name="schools.searschool-en"></s:text> </a></li>
                 
                  
                </ul>

                 <li data-toggle="collapse" data-target="#reports" class="collapsed">
                  <a href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> <s:text name="reports-en"></s:text> <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="reports">
                 <li><a href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i><s:text name="report.dailyreport-en"></s:text></a></li>
                  <li><a href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i><s:text name="report.weeklyreport-en"></s:text></a></li>
                  <li> <a href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i><s:text name="report.monthlyreport-en"></s:text></a></li>
                  <li><a href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i><s:text name="report.yearlyreport-en"></s:text> </a></li>
                  <li><a href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i><s:text name="report.customreport-en"></s:text></a></li>
                  
                </ul>
                
                <li>
                  <a href="#">
                  <i class="fa fa-circle" aria-hidden="true"></i> <s:text name="online"></s:text>
                  </a>
                  </li>
                
                
                
                
                 <li data-toggle="collapse" data-target="#admin" class="collapsed">
                  <a href="#"><i class="fa fa-cog" aria-hidden="true"></i>  <s:text name="settings-en"> </s:text> <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="admin">
                  <li> <a href="#"><i class="fa fa-spoon" aria-hidden="true"></i><s:text name="settings.meals-en"> </s:text></a></li>
                  <li><a href="#"><i class="fa fa-birthday-cake" aria-hidden="true"></i><s:text name="settings.package-en"> </s:text></a></li>
                  <li><a href="#"><i class="fa fa-users" aria-hidden="true"></i><s:text name="settings.usermanagement-en"> </s:text></a></li>
                  <li><a href="#"><i class="fa fa-envelope" aria-hidden="true"></i><s:text name="settings.emailSetting-en"> </s:text></a></li>
                  
                </ul>


                 

                
            </ul>
     </div> 
</div> -->
<!-- Left side menu ends -->
<!-- body starts  -->
 <tiles:insertAttribute name="body" />  
 
 <!-- body starts  -->     
 

 
    </body>  
    </html>   