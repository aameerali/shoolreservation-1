<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib prefix="s" uri="/struts-tags"%>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel='stylesheet' href='css/home.css' />


</head>
<body>
<div class="container">

<div class="row">
                    
                    <div class="col-md-4">
                        
                        <div class="menu-item blue">
                            <a data-toggle="modal" href="#feature-modal">
                                <i class="fa fa-magic"></i>
                                <p><s:text name="reservation-en"></s:text></p>
                                <p ><s:text name="reservation-ar"></s:text></p>
                            </a>
                        </div>
                        
                        
                        
                        <div class="menu-item light-red">
                            <a data-toggle="modal" href="#about-modal">
                                <i class="fa fa-file-pdf-o"></i>
                                <p><s:text name="reports-en"></s:text>  </p>
                                <p><s:text name="reports-ar"></s:text>  </p>
                            </a>
                        </div>
                        
                    </div>
                    
                    <div class="col-md-4">
                               <div class="menu-item green">
                            <a data-toggle="modal" href="#portfolio-modal">
                                <i class="fa fa-university"></i>
                                <p><s:text name="schools-en"></s:text></p>
                                 <p><s:text name="schools-ar"></s:text></p>
                            </a>
                            </div>
                            
                                <div class="menu-item light-orange">
                                    <a  href="/schoolReservation/settings/settingsAction.action">
                                        <i class="fa fa-cog"></i>
                                        <p><s:text name="settings-en"></s:text></p>
                                        <p><s:text name="settings-ar"></s:text></p>
                                    </a>
                            </div>
                      
                    </div>
                    
                    <div class="col-md-4">
                        
                        <div class="menu-item light-red">
                            <a data-toggle="modal" href="#contact-modal">
                                <i class="fa fa-comment-o"></i>
                                <p><s:text name="online-en"></s:text></p>
                                <p><s:text name="online-ar"></s:text></p>
                            </a>
                        </div>
                        
                        
                        
                        <div class="menu-item blue">
                            <a data-toggle="modal" href="#news-modal">
                                <i class="fa fa-edit"></i>
                                <p><s:text name="documents-en"></s:text></p>
                                 <p><s:text name="documents-ar"></s:text></p>
                            </a>
                        </div>
                        
                    </div>
                </div>


</div>



 

 
 
</body>
</html>