<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Login</title>
<link rel="stylesheet" href="css/bootstrap.min.css" />
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/login.css">
<script src="js/html5shiv.min.js"></script>
<script src="js/respond.min.js"></script>
<script src="jquery/jquery-1.11.3.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</head>
<body>
	<!-- PAGE CONTENT -->
	<div class="container" style="margin-top: 10%; margin: 0 auto; width: 480px">
		<img src="images/fakieh_logo.jpg" class="img-responsive" alt="Logo" style="margin: 0 auto; margin-top: 100px; margin-bottom: 50px;">
		<s:if test="hasActionErrors()">
			<div class="text-danger">
				<s:actionerror />
			</div>
		</s:if>
		<div class="panel" style="margin-top: 10%; margin: 0 auto; width: 480px">
			<form role="form" method="post" class="login-form" action="loginAction">
				<section class="text-inputs">
					<input autofocus="" type="text" class="login-input form-control" id="userName" name="userName"
						placeholder="<s:text name='login.userid-en'></s:text> / <s:text
								name='login.userid-ar'></s:text>" required="required"> <input
						type="password" class="login-input form-control" id="password" name="password"
						placeholder="<s:text name='login.password-en'></s:text> / <s:text
								name='login.password-ar'></s:text>" required="required">
				</section>
				<div class="button-container-center">
					<button type="submit" class="btn btn-primary btn-lg">
						<s:text name="login.button-en"></s:text>
						/
						<s:text name="login.button-ar"></s:text>
					</button>
				</div>
				<div class="form-group" style="margin-top: 10px; text-align: center;">
					<label class="big-font"> <a href="forgotpassword"><s:text name="login.forgotpassword-en"></s:text> / <s:text
								name="login.forgotpassword-ar"></s:text></a></label>
				</div>
			</form>
		</div>

		<!--END PAGE CONTENT -->
</body>
</html>