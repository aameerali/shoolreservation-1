<!DOCTYPE html>
<%@ page language="java" contentType="text/html"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Email configuration</title>
<%
	String contextPath = request.getContextPath();
%>
<link href="<%=contextPath%>/css/defaultpages.css" rel="stylesheet">
<style type="text/css">
.form-control {
	max-width: 400px;
}
/* html { */
/* 	 direction: rtl;  */
/* } */
</style>
</head>
<body>
	<div class="container">
		<!--  <ul class="breadcrumb">
        <li class="active">Home</li>
    	</ul>-->
		<h2>
			<s:text name="settings.emailconfig-en"></s:text>
		</h2>
		<s:if test="hasActionErrors()">
			<div class="text-danger">
				<s:actionerror />
			</div>
		</s:if>
		<form id="emailConfigForm" name="emailConfigForm" method="post" action="saveEmailConfig">
			<div class="form-group"></div>
			<div class="form-group">
				<label for="email"><s:text name="settings.emailid-en"></s:text> :</label> <input type="email"
					class="form-control" id="email" name="email" value="<s:property value='email'/>" required>
			</div>
			<div class="form-group">
				<label for="password"><s:text name="settings.emailpassword-en"></s:text> :</label> <input
					type="password" class="form-control" id="password" name="password" value="<s:property value='password'/>" required>
			</div>
			<div class="form-group">
				<label for="host"><s:text name="settings.emailhost-en"></s:text> :</label> <input type="host"
					class="form-control" id="host" name="host" value="<s:property value='host'/>" required>
			</div>
			<div class="form-group">
				<label for="port"><s:text name="settings.emailport-en"></s:text> :</label> <input type="port"
					class="form-control" id="port" name="port" value="<s:property value='port'/>" required>
			</div>
			<button type="submit" class="btn btn-success btn-lg">
				<s:text name="settings.save-en"></s:text>
			</button>
		</form>
	</div>
	</div>
</body>
</html>