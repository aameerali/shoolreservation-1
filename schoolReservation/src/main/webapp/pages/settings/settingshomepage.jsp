<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<%
	String contextPath = request.getContextPath();
%>
<link href="<%=contextPath%>/css/defaultpages.css" rel="stylesheet">
</head>
<body>
	<div class="container">
		<!--  <ul class="breadcrumb">
        <li class="active">Home</li>
    	</ul>-->
		<ul class="nav nav-pills nav-stacked">
			<li><a href="<%=contextPath%>/settings/emailConfigAction"><s:text name="settings.emailconfig-en"></s:text> / <s:text name="settings.emailconfig-ar"></s:text>
			</a></li>
			<li><a href="<%=contextPath%>/settings/userManageAction"><s:text name="settings.usermanagement-en"></s:text> / <s:text
						name="settings.usermanagement-ar"></s:text> </a></li>
		</ul>
	</div>
	</div>
</body>
</html>