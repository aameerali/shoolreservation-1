package settings;

import java.util.List;

import mapping.EmailConfig;

import org.apache.log4j.Logger;

import settings.EmailManageModel;

import settings.EmailManageDao;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class EmailManageAction extends ActionSupport implements ModelDriven<EmailManageModel> {
	private static final Logger log = Logger.getLogger(EmailManageAction.class);
	private EmailManageModel emailModel = new EmailManageModel();
	
	public EmailManageModel getModel() {
		// TODO Auto-generated method stub
		return emailModel;
	}
	
	public String emailConfigDetails() {
		log.info("Executing emailConfig");
		try{
			EmailManageDao emailDao = new EmailManageDao();
			List<EmailConfig> emailList = emailDao.getEmailConfig();
			if(emailList.size() > 0){
				EmailConfig econfig = emailList.get(0);
				emailModel.setEmail(econfig.getEmail());
				emailModel.setHost(econfig.getHost());
				emailModel.setPassword(econfig.getPassword());
				emailModel.setPort(econfig.getPort());
			}
			
		}catch(Exception e){
			e.printStackTrace();
			addActionError(getText("error.general"));
		}
		return SUCCESS;
	}
	
	public String saveEmailConfig(){
		try {
			EmailManageDao emailDao = new EmailManageDao();
			//emailDao.deleteExistEmailConfig();
			//emailDao.saveEmailConfig(getModel());
			emailDao.saveUpdateEmailConig(getModel());
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(getText("error.general"));
		}
		return SUCCESS;
		
	}

}
