package settings;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.ActionSupport;

public class SettingsAction extends ActionSupport {

	private static final Logger log = Logger.getLogger(SettingsAction.class);

	public String execute() {
		log.info("Executing  Settings default method");
		return SUCCESS;
	}

}
