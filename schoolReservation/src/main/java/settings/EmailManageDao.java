package settings;

import java.util.ArrayList;
import java.util.List;

import mapping.EmailConfig;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import Connection.HibernateUtill;
import settings.EmailManageModel;

public class EmailManageDao {

	public List<EmailConfig> getEmailConfig(){
		List<EmailConfig> uselist =new ArrayList<EmailConfig>();
		try{
				Session session = HibernateUtill.getSession();
				Query query = session.createQuery("from EmailConfig  ");
				 uselist = query.list();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return uselist;
	}

	public void deleteExistEmailConfig() throws Exception{
		
			Session session = HibernateUtill.getSession();
			Transaction tx= session.getTransaction();
			tx.begin();
			session.createQuery("delete from EmailConfig ").executeUpdate();
			tx.commit();
		
	}
	
	public void saveEmailConfig(EmailManageModel emodel) throws Exception{
		Session session = HibernateUtill.getSession();
		Transaction tx= session.getTransaction();
		tx.begin();
		EmailConfig econfig = new EmailConfig();
		econfig.setEmail(emodel.getEmail());
		econfig.setHost(emodel.getHost());
		econfig.setPassword(emodel.getPassword());
		econfig.setPort(emodel.getPort());
		session.save(econfig);
		tx.commit();
	}
	
	public void saveUpdateEmailConig(EmailManageModel model) throws Exception {
		deleteExistEmailConfig();
		saveEmailConfig(model);
	}
}
