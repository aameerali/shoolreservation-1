package Login;


import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Query;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import Connection.HibernateUtill;
import mapping.Users;

public class LoginDao {
	
	public Object authenticateUser(LoginModel baseModel){
		String vaildUser = "invalidUser";
		Session session = HibernateUtill.getSession();
		try{
			
			Query query = session.createQuery("from Users where loginId = :id and password = :pwd and active =:active ");
			query.setParameter("id", baseModel.getUserName());
			query.setParameter("pwd", baseModel.getPassword());
			query.setParameter("active", true);
			
			Users user = (Users)query.uniqueResult();
			return user;
		}catch(Exception e){
			return null;
		}
		finally{
			session.close();
		}
	}

}
