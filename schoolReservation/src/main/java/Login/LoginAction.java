package Login;





import java.util.Map;

import mapping.Users;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class LoginAction extends ActionSupport implements ModelDriven<LoginModel>,SessionAware{
	
private Map httpsession;
private LoginDao loginDao;
	
	/**
	 * A getter method for the current session.
	 * @return The current session.
	 */
	    public Map getSession() {
	        return httpsession;
	    }
	/**
	 * A setter method for the current session.
	 * @param session The current session.
	 */
	    public void setSession(Map session) {
	        this.httpsession = session;
	    }

	
	private  LoginModel loginModel  = new LoginModel();
	private static final Logger log = Logger.getLogger(LoginAction.class);
	@Override
	
	public String execute(){
		log.info("Inside login method");
		return SUCCESS;
	}
	
	
	public LoginModel getModel() {
		// TODO Auto-generated method stub
		return loginModel;
	}
	
	
	
	public String login(){
		loginDao  = new LoginDao();
		try {
			Users  user =(Users) loginDao.authenticateUser(getModel());
			if(user == null){
				addActionError(getText("login.error.invalidcrd"));
				return INPUT;
			}else{
				httpsession  =  ActionContext.getContext().getSession();
				httpsession.put("userFname", user.getFname().toUpperCase());
				httpsession.put("userLname", user.getLname().toUpperCase());
				httpsession.put("loginId",user.getLoginId());
				httpsession.put("role",user.getRole());
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(getText("login.error.dataBaseConn"));
			return INPUT;
		}
		
		return SUCCESS;
	}
	
	public String homePage(){
	return SUCCESS;	
	}

}
